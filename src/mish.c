#include <mish.h>
#include <mishdict.h>
#include <mishutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pwd.h>
#ifdef __linux__
#include <bsd/vis.h>
#else
#include <vis.h>
#endif
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/wait.h>

pid_t current_pid = 0;
int jump_line;
extern int current_line;

/*
 * Set the variable.
 */
void mish_set_variable(char* key, char* value){
}

/*
 * Test prompt
 * char* _prompt = "%{\\e[0m}[%{\\e[1m\\e[32m}%u%{\\e[0m}@%{\\e[1m\\e[31m}%h%{\\e[0m}:%{\\e[1m\\e[34m}%~%{\\e[0m}]%$ ";
 */

const char* _prompt = "%s!%h%p ";

/*
 * Parse the prompt.
 */
char* mish_parse_prompt(){
	char* result = malloc(1);
	result[0] = 0;
	bool percent = false;
	bool bracket = false;
	bool backslash = false;
	int i;
	char* actual_prompt = getenv("prompt");
	if(actual_prompt == NULL) actual_prompt = (char*)_prompt;
	for(i = 0; i < strlen(actual_prompt); i++){
		char ch = actual_prompt[i];
		if(bracket){
			if(backslash){
				char addch = 0;
				if(ch == 'e'){
					addch = 0x1b;
				}else if(ch == 'a'){
					addch = 0x07;
				}else if(ch == 'r'){
					addch = '\r';
				}else if(ch == 'n'){
					addch = '\n';
				}
				if(addch != 0){
					char* tmp = result;
					char* tmp2 = malloc(2);
					tmp2[1] = 0;
					tmp2[0] = addch;
					result = sstrcat(tmp, tmp2);
					free(tmp2);
					free(tmp);
					backslash = false;
				}
			}else{
				if(ch == '}'){
					bracket = false;
				}else if(ch == '\\'){
					backslash = true;
				}else{
					char* tmp = result;
					char* tmp2 = malloc(2);
					tmp2[1] = 0;
					tmp2[0] = ch;
					result = sstrcat(tmp, tmp2);
					free(tmp2);
					free(tmp);
					backslash = false;
				}
			}
			continue;
		}
		if(percent){
			if(ch == 'h'){
				char* hostname = malloc(128);
				if(gethostname(hostname, 128) == -1){
					free(hostname);
					hostname = nstrdup("Amnesiac");
				}
				char* tmp = result;
				result = sstrcat(tmp, hostname);
				free(tmp);
				free(hostname);
			}else if(ch == 'u'){
				struct passwd* pwdent = getpwuid(getuid());
				char* username = pwdent->pw_name;
				char* tmp = result;
				result = sstrcat(tmp, username);
				free(tmp);
			}else if(ch == 's'){
				char* tmp = result;
				result = sstrcat(tmp, "mish");
				free(tmp);
			}else if(ch == 'p'){
				char* tmp = result;
				char* tmp2 = malloc(2);
				tmp2[1] = 0;
				tmp2[0] = getuid() == 0 ? '#' : '$';
				result = sstrcat(tmp, tmp2);
				free(tmp2);
				free(tmp);
			}else if(ch == '~'){
				char* cwd = NULL;
				char* orig_cwd = NULL;
				if((cwd = getcwd(cwd, 0)) == NULL){
					cwd = nstrdup("?");
				}
				orig_cwd = cwd;
				struct passwd* pwdent = getpwuid(getuid());
				if(nstarts_with(cwd, pwdent->pw_dir)){
					cwd += strlen(pwdent->pw_dir) - 1;
					cwd[0] = '~';
				}
				char* tmp = result;
				result = sstrcat(tmp, cwd);
				free(tmp);
				free(orig_cwd);
			}else if(ch == '{'){
				bracket = true;
			}
			percent = false;
			continue;
		}
		if(ch == '%'){
			percent = true;
		}else{
			char* tmp = result;
			char* tmp2 = malloc(2);
			tmp2[1] = 0;
			tmp2[0] = ch;
			result = sstrcat(tmp, tmp2);
			free(tmp2);
			free(tmp);
		}
	}
	return result;
}

/*
 * Parse the PATH
 */
struct mish_dict_element** mish_path_dict = NULL;
void mish_parse_path(){
	if(mish_path_dict == NULL){
		mish_path_dict = mish_dict_create();
	}
	int i;	
	for(i = 0; mish_path_dict[i] != NULL; i++){
		free(mish_path_dict[i]->value);
	}
	mish_path_dict = mish_dict_delete_all(mish_path_dict);
	char* buffer = malloc(1);
	buffer[0] = 0;
	char* path = getenv("PATH");
	int counter = 0;
	for(i = 0; i < strlen(path); i++){
		char ch = path[i];
		if(ch == ':'){
			/*printf("PATH: +%s\n", buffer);*/
			mish_path_dict = mish_dict_add(mish_path_dict, "P", nstrdup(buffer));
			free(buffer);
			buffer = malloc(1);
			buffer[0] = 0;
			counter = 0;
		}else{
			buffer = realloc(buffer, counter + 2);
			buffer[counter] = ch;
			buffer[counter + 1] = 0;
			counter++;
		}
	}
	if(strlen(buffer) != 0){
		/*printf("PATH: +%s\n", buffer);*/
		mish_path_dict = mish_dict_add(mish_path_dict, "P", nstrdup(buffer));
		free(buffer);
		buffer = malloc(1);
		buffer[0] = 0;
	}
	free(buffer);
}
void h(int);
int skip_cond = 0;
int skip_loop = 0;
int* line = NULL;

#define FREE_ARGV	{\
				int argv_counter;\
				for(argv_counter = 0; orig_argv[argv_counter] != NULL; argv_counter++){\
					free(orig_argv[argv_counter]);\
				}\
				free(orig_argv);\
			}

/*
 * Run the command.
 */
int mish_run_command(char* command){
	if(line == NULL){
		line = malloc(sizeof(*line));
		line[0] = -1;
	}
	jump_line = -1;
	char* encoded = malloc(strlen(command) * 4 + 1);
	strvis(encoded, command, VIS_CSTYLE | VIS_SAFE | VIS_NOSLASH);
	char** argv = malloc(sizeof(*argv));
	argv[0] = NULL;
	int i;
	int counter = 0;
	bool dq = false; /* Double Quotation */
	bool sq = false; /* Single Quotation */
	bool bs = false; /* Backslash */
	bool dl = false; /* Dollar */
	bool at = false; /* AtMark */
	bool sb = false; /* Square Bracket */
	bool bt = false; /* Backtick */
	char* buffer = malloc(1);
	buffer[0] = 0;
	char* variable_buffer = malloc(1);
	variable_buffer[0] = 0;
	char* backtick_buffer = malloc(1);
	backtick_buffer[0] = 0;
	for(i = 0; i < strlen(encoded); i++){
		char ch = encoded[i];
		if(bt){
			if(bs){
				char* tmp = backtick_buffer;
				char* tmp2 = malloc(2);
				tmp2[0] = ch;
				tmp2[1] = 0;
				backtick_buffer = sstrcat(tmp, tmp2);
				free(tmp);
				free(tmp2);
				bs = false;
			}else{
				if(ch == '`'){
					int* std_fd = malloc(sizeof(*std_fd) * 2);
					pipe(std_fd);
					pid_t pid = fork();
					if(pid == 0){
						close(std_fd[0]);
						dup2(std_fd[1], 1);
						int status = mish_run_command(backtick_buffer);
						close(std_fd[1]);
						_exit(status);
					}else{
						close(std_fd[1]);
						int status;
						waitpid(pid, &status, 0);
						char* tmp = malloc(2);
						tmp[1] = 0;
						while(read(std_fd[0], tmp, 1) > 0){
							char* tmp2 = buffer;
							buffer = sstrcat(tmp2, tmp);
							free(tmp2);
						}
						free(tmp);
					}
					free(backtick_buffer);
					backtick_buffer = malloc(1);
					backtick_buffer[0] = 0;
					bt = false;
				}else if(ch == '\\'){
					bs = true;
				}else{
					char* tmp = backtick_buffer;
					char* tmp2 = malloc(2);
					tmp2[0] = ch;
					tmp2[1] = 0;
					backtick_buffer = sstrcat(tmp, tmp2);
					free(tmp);
					free(tmp2);
				}
			}
			continue;
		}

		if(at){
			if(ch == '@'){
				char* data = getenv(variable_buffer);
				char* tmp = buffer;
				char* value = (data == NULL ? nstrdup("") : nstrdup(data));
				buffer = sstrcat(tmp, value);
				free(value);
				free(tmp);
				at = false;
				free(variable_buffer);
				variable_buffer = malloc(1);
				variable_buffer[0] = 0;
			}else{
				char* tmp = variable_buffer;
				char* tmp2 = malloc(2);
				tmp2[1] = 0;
				tmp2[0] = ch;
				variable_buffer = sstrcat(tmp, tmp2);
				free(tmp2);
				free(tmp);
			}
			continue;
		}
		if(dl){
			if(ch == '$'){
				char* data = getenv(variable_buffer);
				char* tmp = buffer;
				char* value = (data == NULL ? nstrdup("") : nstrdup(data));
				buffer = sstrcat(tmp, value);
				free(value);
				free(tmp);
				dl = false;
				free(variable_buffer);
				variable_buffer = malloc(1);
				variable_buffer[0] = 0;
			}else{
				char* tmp = variable_buffer;
				char* tmp2 = malloc(2);
				tmp2[1] = 0;
				tmp2[0] = ch;
				variable_buffer = sstrcat(tmp, tmp2);
				free(tmp2);
				free(tmp);
			}
			continue;
		}
		if(bs){
			char* tmp = buffer;
			char* tmp2;
			char addch = 0;
		       	if(ch == 'n'){
				addch = '\n';
			}else if(ch == '[' || ch == ']' || ch == '@' || ch == '$'){
				addch = ch;
			}else{
				tmp2 = malloc(3);
				tmp2[0] = '\\';
				tmp2[1] = ch;
				tmp2[2] = 0;
			}
			if(addch != 0){
				tmp2 = malloc(2);
				tmp2[0] = addch;
				tmp2[1] = 0;
			}
			buffer = sstrcat(tmp, tmp2);
			free(tmp);
			free(tmp2);
			bs = false;
			continue;
		}
		if(ch == '\\'){
			bs = true;
		}else if(sb){
			if(at){
				if(ch == '@'){
					char* data = getenv(variable_buffer);
					char* tmp = buffer;
					char* value = (data == NULL ? nstrdup("") : nstrdup(data));
					buffer = sstrcat(tmp, value);
					free(value);
					free(tmp);
					at = false;
					free(variable_buffer);
					variable_buffer = malloc(1);
					variable_buffer[0] = 0;
				}else{
					char* tmp = variable_buffer;
					char* tmp2 = malloc(2);
					tmp2[1] = 0;
					tmp2[0] = ch;
					variable_buffer = sstrcat(tmp, tmp2);
					free(tmp2);
					free(tmp);
				}
				continue;
			}
			if(dl){
				if(ch == '$'){
					char* data = getenv(variable_buffer);
					char* tmp = buffer;
					char* value = (data == NULL ? nstrdup("") : nstrdup(data));
					buffer = sstrcat(tmp, value);
					free(value);
					free(tmp);
					dl = false;
					free(variable_buffer);
					variable_buffer = malloc(1);
					variable_buffer[0] = 0;
				}else{
					char* tmp = variable_buffer;
					char* tmp2 = malloc(2);
					tmp2[1] = 0;
					tmp2[0] = ch;
					variable_buffer = sstrcat(tmp, tmp2);
					free(tmp2);
					free(tmp);
				}
				continue;
			}

			if(ch == ']'){
				char* tmp = buffer;
				buffer = sstrcat(tmp, "]");
				free(tmp);
				sb = false;
			}else if(ch == '"' && !sq){
				char* tmp = buffer;
				buffer = sstrcat(tmp, "\"");
				free(tmp);
				dq = !dq;
			}else if(ch == '\'' && !dq){
				char* tmp = buffer;
				buffer = sstrcat(tmp, "'");
				free(tmp);
				sq = !sq;
			}else if((ch == ' ' || ch == '\t') && (!sq && !dq)){
				argv = realloc(argv, sizeof(*argv) * (counter + 2));
				argv[counter] = nstrdup(buffer);
				argv[counter + 1] = NULL;
				free(buffer);
				buffer = malloc(1);
				buffer[0] = 0;
				counter++;
			}else if(ch == '@'){
				at = true;
			}else if(ch == '$'){
				dl = true;
			}else{
				char* tmp = buffer;
				char* tmp2 = malloc(2);
				tmp2[0] = ch;
				tmp2[1] = 0;
				buffer = sstrcat(tmp, tmp2);
				free(tmp);
				free(tmp2);
			}
		}else if(ch == '"' && sq){
			char* tmp = buffer;
			buffer = sstrcat(tmp, "\"");
			free(tmp);
		}else if(ch == '\'' && dq){
			char* tmp = buffer;
			buffer = sstrcat(tmp, "'");
			free(tmp);
		}else if(ch == '"'){
			dq = !dq;
		}else if(ch == '\''){
			sq = !sq;
		}else if(ch == '`'){
			bt = true;
		}else if(ch == ' ' && (sq || dq)){
			char* tmp = buffer;
			buffer = sstrcat(tmp, " ");
			free(tmp);
		}else if(ch == '\t' && (sq || dq)){
			char* tmp = buffer;
			buffer = sstrcat(tmp, "\t");
			free(tmp);
		}else if(ch == ' ' || ch == '\t'){
			argv = realloc(argv, sizeof(*argv) * (counter + 2));
			argv[counter] = nstrdup(buffer);
			argv[counter + 1] = NULL;
			free(buffer);
			buffer = malloc(1);
			buffer[0] = 0;
			counter++;
		}else if(ch == '['){
			char* tmp = buffer;
			buffer = sstrcat(tmp, "[");
			free(tmp);
			sb = true;
		}else if(ch == '@'){
			at = true;
		}else if(ch == '$'){
			dl = true;
		}else if(ch == '~' && !sq && !dq){
			char* tmp = buffer;
			struct passwd* pwdent = getpwuid(getuid());
			buffer = sstrcat(tmp, pwdent->pw_dir);
			free(tmp);
		}else{
			char* tmp = buffer;
			char* tmp2 = malloc(2);
			tmp2[0] = ch;
			tmp2[1] = 0;
			buffer = sstrcat(tmp, tmp2);
			free(tmp);
			free(tmp2);
		}
	}
	if(strlen(buffer) != 0){
		argv = realloc(argv, sizeof(*argv) * (counter + 2));
		argv[counter] = nstrdup(buffer);
		argv[counter + 1] = NULL;
		free(buffer);
		buffer = malloc(1);
		buffer[0] = 0;
		counter++;
	}
	free(buffer);
	free(variable_buffer);
	free(backtick_buffer);
	char** orig_argv = argv;
	if(counter != 0){
		int i;
		int slide = 0;
		for(i = 0; i < counter; i++){
			if(strlen(argv[i]) == 0){
				slide++;
			}else{
				break;
			}
		}
		argv += slide;
		counter -= slide;
		if(skip_cond){
			if(strcmp(argv[0], "}") == 0){
				skip_cond--;
			}else{
				for(i = 0; i < counter; i++){
					if(strcmp(argv[i], "{") == 0){
						skip_cond++;
					}
				}
			}
			FREE_ARGV;
			return 0;
		}
		if(skip_loop){
			if(strcmp(argv[0], "}") == 0){
				skip_loop--;
				jump_line = line[0];
				int index;
				for(index = 0; line[index] != -1; index++);
				int* tmp_line = malloc(sizeof(*tmp_line) * (index));
				for(i = 1; i < index; i++){
					line[i - 1] = line[i];
					line[i] = -1;
				}
				FREE_ARGV;
				return 0;
			}
		}
		if(strcmp(argv[0], "exit") == 0){
			if(counter > 1){
				exit(atoi(argv[1]));
			}else{
				exit(0);
			}
		}else if(strcmp(argv[0], "repath") == 0){
			mish_parse_path();
		}else if(strcmp(argv[0], "cd") == 0 || strcmp(argv[0], "chdir") == 0){
			if(argv[1] == NULL){
				struct passwd* pwdent = getpwuid(getuid());
				chdir(pwdent->pw_dir);
			}else{
				if(chdir(argv[1]) != 0){
					fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
				}
			}
		}else if(strcmp(argv[0], "setenv") == 0){
			if(counter == 3){
				setenv(argv[1], argv[2], 1);
			}else{
				fprintf(stderr, "%s: usage: setenv key value\n", argv[0]);
			}
		}else if(strcmp(argv[0], "unsetenv") == 0){
			if(counter == 2){
				unsetenv(argv[1]);
			}else{
				fprintf(stderr, "%s: usage: unsetenv key\n", argv[0]);
			}
		}else if(strcmp(argv[0], "if") == 0){
			char* str = malloc(1);
			str[0] = 0;
			for(i = 1; argv[i] != NULL; i++){
				if(strcmp(argv[i], "{") != 0){
					char* tmp = str;
					str = sstrcat3(tmp, " ", argv[i]);
					free(tmp);
				}
			}
			int status = mish_run_command(str);
			if(status != 0){
				skip_cond++;
			}
			free(str);
		}else if(strcmp(argv[0], "while") == 0){
			char* str = malloc(1);
			str[0] = 0;
			for(i = 1; argv[i] != NULL; i++){
				if(strcmp(argv[i], "{") != 0){
					char* tmp = str;
					str = sstrcat3(tmp, " ", argv[i]);
					free(tmp);
				}
			}
			int status = mish_run_command(str);
			if(status != 0){
				skip_cond++;
			}else{
				skip_loop++;
				int index;
				for(index = 0; line[index] != -1; index++);
				line = realloc(line, sizeof(*line) * (index + 2));
				if(index == 0) line[1] = -1;
				for(i = 0; i < index; i++){
					line[i + 1] = line[i];
					line[i + 2] = -1;
				}
				line[0] = current_line + 1;
			}
			free(str);
		}else if(strcmp(argv[0], "}") == 0){
			skip_cond = 0;
		}else if(argv[0][0] == '#'){
		}else{
			char* run_path = nstrdup(argv[0]);
			struct stat s;
			if(stat(run_path, &s) != 0 && argv[0][0] != '.'){
				for(i = 0; mish_path_dict[i] != NULL; i++){
					char* try_path = sstrcat3(mish_path_dict[i]->value, "/", argv[0]);
					if(stat(try_path, &s) == 0){
						free(run_path);
						run_path = try_path;
						break;
					}
					free(try_path);
				}
			}
			pid_t pid = fork();
			if(pid == 0){
				execv(run_path, argv);
				int s = errno;
				fprintf(stderr, "%s: %s\n", argv[0], strerror(s));
				free(run_path);
				_exit(s);
			}else{
				current_pid = pid;
				int status;
				waitpid(pid, &status, 0);
				current_pid = 0;
				int s = WEXITSTATUS(status);
				char* exit_status = malloc(1);
				exit_status[0] = 0;
				int orig_status = s;
				while(1){
					char ch = '0' + (s % 10);
					s /= 10;
					char* tmp = exit_status;
					char* tmp2 = malloc(2);
					tmp2[0] = ch;
					tmp2[1] = 0;
					exit_status = sstrcat(tmp2, tmp);
					free(tmp);
					free(tmp2);
					if(s == 0) break;
				}
				setenv("EXIT", exit_status, 1);
				setenv("RUN", argv[0], 1);
				setenv("EXEC", run_path, 1);
				free(exit_status);
				free(run_path);
				if(WIFSIGNALED(status)){
					if(WTERMSIG(status) == SIGSEGV){
						fprintf(stderr, "Segmentaion fault\n");
					}
				}
				FREE_ARGV;
				return orig_status;
			}
		}
	}
	free(encoded);
	FREE_ARGV;
	return -1;
}
