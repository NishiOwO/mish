#include <mishutil.h>
#include <string.h>
#include <stdlib.h>

/*
 * Secure implementation of strcat.
 */
char* sstrcat(const char* str1, const char* str2){
	char* result = malloc(strlen(str1) + strlen(str2) + 1);
	result[strlen(str1) + strlen(str2)] = 0;
	strcpy(result, str1);
	strcpy(result + strlen(str1), str2);
	return result;
}

/*
 * sstrcat, but 3 strings.
 */
char* sstrcat3(const char* str1, const char* str2, const char* str3){
	char* tmp = sstrcat(str1, str2);
	char* result = sstrcat(tmp, str3);
	free(tmp);
	return result;
}

/*
 * strdup implementation.
 */
char* nstrdup(const char* str){
	char* result = malloc(strlen(str) + 1);
	result[strlen(str)] = 0;
	strcpy(result, str);
	return result;
}

/*
 * Check the string if it starts with some string.
 */
bool nstarts_with(const char* orig, const char* starts_with){
	if(strlen(orig) < strlen(starts_with)) return false;
	int i;
	for(i = 0; i < strlen(starts_with); i++){
		if(starts_with[i] != orig[i]) return false;
	}
	return true;
}
