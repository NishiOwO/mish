#include <mishdict.h>
#include <stdlib.h>
#include <string.h>

struct mish_dict_element** mish_dict_create(){
	struct mish_dict_element** dict = malloc(sizeof(*dict));
	dict[0] = NULL;
	return dict;
}

struct mish_dict_element** mish_dict_add(struct mish_dict_element** dict, char* key, void* value){
	int i;
	for(i = 0; dict[i] != NULL; i++);
	dict = realloc(dict, sizeof(*dict) * (i + 2));
	struct mish_dict_element* element = malloc(sizeof(*element));
	element->value = value;
	element->key = malloc(strlen(key) + 1);
	element->key[strlen(key)] = 0;
	strcpy(element->key, key);
	dict[i] = element;
	dict[i + 1] = NULL;
	return dict;
}

struct mish_dict_element* mish_dict_get(struct mish_dict_element** dict, char* key){
	struct mish_dict_element* result = NULL;
	int i;
	for(i = 0; dict[i] != NULL; i++){
		if(strcmp(dict[i]->key, key) == 0){
			result = dict[i];
			break;
		}
	}
	return result;
}

struct mish_dict_element** mish_dict_delete_all(struct mish_dict_element** dict){
	int i;
	for(i = 0; dict[i] != NULL; i++){
		free(dict[i]->key);
		free(dict[i]);
	}
	dict = realloc(dict, sizeof(*dict));
	dict[0] = NULL;
	return dict;
}
