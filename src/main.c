#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <fcntl.h>

#include <mishdict.h>
#include <mishutil.h>
#include <mish.h>
#ifdef __linux__
#include <bsd/vis.h>
#else
#include <vis.h>
#endif

extern int jump_line;
extern pid_t current_pid;
int current_line;

void sigint_child(int sig){
	if(current_pid != 0) kill(current_pid, SIGINT);
}

void run_command(char* str){
	char* encoded = malloc(strlen(str) * 4 + 1);
	strvis(encoded, str, VIS_CSTYLE | VIS_SAFE | VIS_NOSLASH);
	int def_stdin = dup(0);
	int def_stdout = dup(1);
	bool dq = false;
	bool sq = false;
	bool bs = false;
	int i;
	int start = 0;
	int length = 0;
	char* save_buffer;
	for(i = 0; i < strlen(encoded); i++){
		char ch = encoded[i];
		length++;
		if(bs){
			bs = false;
			continue;
		}
		if(ch == '\\'){
			bs = true;
		}else if(ch == '"'){
			dq = !dq;
		}else if(ch == '\''){
			sq = !sq;
		}else if(ch == ';'){
			char* command = malloc(length);
			command[length - 1] = 0;
			memcpy(command, encoded + start, length - 1);
			mish_run_command(command);
			free(command);
			start = i + 1;
			length = 0;
		}
	}
	if(length != 0){
		char* command = malloc(length + 1);
		command[length] = 0;
		memcpy(command, encoded + start, length);
		mish_run_command(command);
		free(command);
		start = i + 1;
		length = 0;
	}
	dup2(def_stdin, 0);
	dup2(def_stdout, 1);
	free(encoded);
}

void run_file(char** argv, int i, FILE* alrdyopen){
	char* str = NULL;
	size_t size = 0;
	ssize_t length;
	FILE* f;
	if(alrdyopen == NULL){
		f = fopen(argv[i], "r");
	}else{
		f = alrdyopen;
	}
	if(f == NULL){
		fprintf(stderr, "%s: %s: %s\n", argv[0], argv[i], strerror(errno));
		exit(1);
	}
	int count = 0;
	while((length = getline(&str, &size, f)) != -1){
		if(jump_line != -1 && count != jump_line){
			count++;
			continue;
		}
		if(jump_line != -1) count--;
		if(str[length - 1] == '\n') str[length - 1] = 0;
		current_line = count;
		run_command(str);
		count++;
		size = 0;
		free(str);
		str = NULL;
		if(jump_line != -1){
			fseek(f, 0, SEEK_SET);
			count = 0;
		}
	}
	exit(0);
}

int main(int argc, char** argv){
	signal(SIGINT, sigint_child);
	signal(SIGTSTP, SIG_IGN);
	setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin", 0);
	setenv("SHELL", "mish", 1);
	struct passwd* pwdent = getpwuid(getuid());
	setenv("USER", pwdent->pw_name, 1);
	setenv("HOME", pwdent->pw_dir, 1);
	{
		char* hostname = malloc(128);
		if(gethostname(hostname, 128) == -1){
			free(hostname);
			hostname = nstrdup("Amnesiac");
		}
		setenv("HOST", hostname, 1);
		free(hostname);
	}
	int i;
	for(i = 1; i < argc; i++){
		if(strlen(argv[i]) > 0){
			if(argv[i][0] == '-'){
				if(strcmp(argv[i], "-V") == 0 || strcmp(argv[i], "--version") == 0){
					printf("mish %.3f\n", MISH_VERSION);
					if(isatty(fileno(stdout))){
	
						printf("Copyright (C) 2023 NishiOwO.\n");
						printf("mish is licensed under the 3-clause BSD License.\n");
					}
					exit(0);
				}
			}else{
				mish_parse_path();
				run_file(argv, i, NULL);
			}
		}
	}
	mish_parse_path();
	if(!isatty(fileno(stdin))){
		run_file(argv, 0, stdin);
	}
	mish_set_variable("prompt", "%u@%h%$");
	char* path = sstrcat(pwdent->pw_dir, "/.mishrc");
	FILE* f = fopen(path, "r");
	if(f != NULL){
		char* str = NULL;
		size_t size = 0;
		ssize_t length;
		int count = 0;
		while((length = getline(&str, &size, f)) != -1){
			if(jump_line != -1 && count != jump_line){
				count++;
				continue;
			}
			if(jump_line != -1) count--;
			if(str[length - 1] == '\n') str[length - 1] = 0;
			current_line = count;
			run_command(str);
			count++;
			size = 0;
			free(str);
			str = NULL;
			if(jump_line != -1){
				fseek(f, 0, SEEK_SET);
				count = 0;
			}
		}
		fclose(f);
	}
	while(1){
		char* parsed_prompt = mish_parse_prompt();
		printf(parsed_prompt);
		free(parsed_prompt);
		char* str = NULL;
		size_t size = 0;
		ssize_t length;
		int count = 0;
		if((length = getline(&str, &size, stdin)) != -1){
			if(jump_line != -1 && count != jump_line){
				count++;
				continue;
			}
			if(str[length - 1] == '\n') str[length - 1] = 0;
			run_command(str);
			free(str);
			if(jump_line != -1){
				fseek(f, 0, SEEK_SET);
				count = 0;
			}
		}else{
			printf("exit\n");
			exit(-1);
		}
	}
}
