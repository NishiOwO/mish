#ifndef __MISH_MSH_H__
#define __MISH_MSH_H__
#define MISH_VERSION 1.002

void mish_set_variable(char* key, char* value);
char* mish_parse_prompt();
int mish_run_command(char* command);
void mish_parse_path();

#endif
