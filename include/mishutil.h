#ifndef __MISH_MSHUTIL_H__
#define __MISH_MSHUTIL_H__

#include <stdbool.h>

char* nstrdup(const char* str);
char* sstrcat(const char* str1, const char* str2);
char* sstrcat3(const char* str1, const char* str2, const char* str3);
bool nstarts_width(const char* orig, const char* starts_with);

#endif
