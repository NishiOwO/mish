#ifndef __MISH_MSHDICT_H__
#define __MISH_MSHDICT_H__

struct mish_dict_element {
	char* key;
	void* value;
};

struct mish_dict_element** mish_dict_create();
struct mish_dict_element** mish_dict_add(struct mish_dict_element** dict, char* key, void* value);
struct mish_dict_element* mish_dict_get(struct mish_dict_element**, char* key);
struct mish_dict_element** mish_dict_delete_all(struct mish_dict_element**);

#endif
