CC := cc
CFLAGS := -std=c89 -I./include -g -fPIC
CFLAGS-LINUX := 
ifeq ($(shell uname -s), Linux)
CFLAGS := $(CFLAGS) -lbsd
CFLAGS-LINUX := /usr/lib/libbsd.a /usr/lib/libbsd-ctor.a
endif

BIN := ./bin
OBJ := ./obj
INC := ./include
SRC := ./src

PREFIX := /usr

all: $(BIN)/mish $(BIN)/mish-static

$(BIN)/mish: $(OBJ)/main.o $(OBJ)/mish.o $(OBJ)/mishdict.o $(OBJ)/mishutil.o
	$(CC) $(CFLAGS) -o $@ $^

$(BIN)/mish-static: $(OBJ)/main.o $(OBJ)/mish.o $(OBJ)/mishdict.o $(OBJ)/mishutil.o
	$(CC) -static $(CFLAGS-LINUX) $(CFLAGS) -o $@ $^

$(OBJ)/main.o: $(SRC)/main.c $(INC)/mish.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJ)/mish.o: $(SRC)/mish.c $(INC)/mish.h $(INC)/mishdict.h $(INC)/mishutil.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJ)/mishdict.o: $(SRC)/mishdict.c $(INC)/mishdict.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJ)/mishutil.o: $(SRC)/mishutil.c $(INC)/mishutil.h
	$(CC) $(CFLAGS) -c -o $@ $<

root:
ifneq ($(shell id -u), 0)
	@echo This target must be run as root.
	@exit 1
endif

install: root $(BIN)/mish
	cp $(BIN)/mish $(PREFIX)/bin/
